//
//  LoginViewController.swift
//  todo-list
//
//  Created by Aleš Kocur on 27/11/15.
//  Copyright © 2015 Aleš Kocur. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var loginButtonActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.usernameTextField.delegate = self
        self.passwordTextField.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    
    @IBAction func login(sender: AnyObject) {
        
        self.loginButton.userInteractionEnabled = false
        self.loginButtonActivityIndicator.startAnimating()
        
        guard let username = self.usernameTextField.text, let password = self.passwordTextField.text where username != "" && password != "" else {
            self.showAlert(NSLocalizedString("Missing username or password", comment: "Alert error title for missing username or password"), text: NSLocalizedString("Fill correct username and password please.", comment: "Alert error text for missing username or password"), dismissButton: NSLocalizedString("Ok", comment: "All correct cancel button"))
            self.loginButton.userInteractionEnabled = true
            self.loginButtonActivityIndicator.stopAnimating()
            return
        }
        
        UserManager.login(username, password: password) { userData -> () in
            
            switch userData {
            case .Data:
                self.performSegueWithIdentifier("showTasks", sender: sender)
            case .Error(let error):
                self.showAlertByError(error)
            }
            
            self.loginButton.userInteractionEnabled = true
            self.loginButtonActivityIndicator.stopAnimating()
        }
        
    }
    
    
    // MARK: - Helpers
    
    /// Shows alert by given error
    func showAlertByError(error: ErrorType) {
        switch error {
        case let e as UserServicesError where e == UserServicesError.BadCredentials:
            self.showAlert(NSLocalizedString("Bad credentials", comment: "Alert error title for bad credentials"), text: NSLocalizedString("Please check your username and password and try again", comment: "Alert error title for bad credentials"), dismissButton: NSLocalizedString("Ok", comment: "All correct cancel button"))
        default:
            self.showAlert(NSLocalizedString("Unexpected error", comment: "Alert error title for unexpected error type"), text: NSLocalizedString("Please check internet connection or try later", comment: "Alert error text for unexpected error type"), dismissButton: NSLocalizedString("Ok", comment: "All correct cancel button"))
        }
    }
    
    func showAlert(title: String, text: String, dismissButton: String) -> UIAlertController {
        let alertController = UIAlertController(title: title, message: text, preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addAction(UIAlertAction(title: dismissButton, style: UIAlertActionStyle.Cancel, handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
        
        return alertController
    }
    
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField === self.usernameTextField {
            self.passwordTextField.becomeFirstResponder()
            return false
        } else if textField == self.passwordTextField {
            self.login(textField)
            return false
        }
        
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
