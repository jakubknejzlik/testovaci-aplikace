//
//  APIClient.swift
//  todo-list
//
//  Created by Aleš Kocur on 27/11/15.
//  Copyright © 2015 Aleš Kocur. All rights reserved.
//

import Alamofire

enum ResponseData<T> {
    case Data(T)
    case Error(ErrorType)
}

class APIClient {
    
    private static let baseURL = "http://api.taskstest.thefuntasty.com"
    
    class func request(method: Alamofire.Method, URLString: String, parameters: [String : AnyObject]? = nil, encoding: ParameterEncoding = .JSON, headers: [String : String]? = nil) -> Request {
        return Alamofire.request(method, APIClient.baseURL + URLString, parameters: parameters, encoding: encoding, headers: headers)
    }
}
