//
//  NSManagedObject+ContextOperations.swift
//  
//
//  Created by Aleš Kocur on 24/11/15.
//  Copyright © 2015 The Funtasty. All rights reserved.
//

import CoreData

extension NSManagedObject {

    func inContext(anotherContext: NSManagedObjectContext) -> NSManagedObject? {
        
        var inContext : NSManagedObject?
        do {
            inContext = try anotherContext.existingObjectWithID(self.objectID)
        } catch {
            return nil
        }
        
        return inContext
    }
}

extension CollectionType where Generator.Element: NSManagedObject {
    
    func inContext(anotherContext: NSManagedObjectContext) -> [NSManagedObject]? {
        return self.map { $0.inContext(anotherContext) as? Generator.Element }.flatMap { $0 }
    }
}