//
//  UserServices.swift
//  todo-list
//
//  Created by Aleš Kocur on 27/11/15.
//  Copyright © 2015 Aleš Kocur. All rights reserved.
//

import JASON
import AERecord

enum UserServicesError: ErrorType {
    case BadCredentials
    case EmptyResponse
    case ContextError
}

struct UserBox {
    let user: User
    let token: UserToken
}

typealias UserToken = String

class UserServices {
    
    class func login(username username: String, password: String, completion: (ResponseData<UserBox>) -> ()) {
        
        let params: [String: AnyObject] = ["username": username, "password": password]
        
        APIClient.request(.POST, URLString: "/authorize", parameters: params, encoding: .JSON).responseJSON { response -> Void in
            
            if let res = response.response where res.statusCode == 400 {
                completion(.Error(UserServicesError.BadCredentials))
                return
            }
            
            guard let jsonData = response.result.value else {
                completion(.Error(UserServicesError.EmptyResponse))
                return
            }
            
            let json = JSON(jsonData)
            
            let user: User? = Decoder.decode(json["user"].object, context: AERecord.backgroundContext)
            
            if let user = user, let token = json["token"].string {
                AERecord.saveContextAndWait(user.managedObjectContext)
                
                if let userInDefaultContext = user.inContext(AERecord.defaultContext) as? User {
                    completion(.Data(UserBox(user: userInDefaultContext, token: token)))
                } else {
                    completion(.Error(UserServicesError.ContextError))
                }
                
            } else {
                completion(.Error(UserServicesError.ContextError))
            }
            
        }
    }
    
}

