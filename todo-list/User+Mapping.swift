//
//  User+Mapping.swift
//  todo-list
//
//  Created by Aleš Kocur on 27/11/15.
//  Copyright © 2015 Aleš Kocur. All rights reserved.
//

import AERecord
import JASON
import CoreData

extension User: DecodableOnContext {
    
    @nonobjc static let primaryKey = "id"
    
    static func primaryKeyValueFromJSON(json: JSON) -> AnyObject? {
        return json["id"].int
    }
    
    static func firstOrCreateWithPrimaryKeyValue(primaryKey: String, value: AnyObject, context: NSManagedObjectContext) -> User {
        return User.firstOrCreateWithAttribute(primaryKey, value: value, context: context) as! User
    }
    
    static func decode(object: User, json: JSON, context: NSManagedObjectContext) {
        object.username <? json["username"]
    }
    
}
