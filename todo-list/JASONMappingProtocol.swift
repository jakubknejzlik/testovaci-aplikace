//
//  JASON+Context.swift
//
//
//  Created by Aleš Kocur on 19/11/15.
//  Copyright © 2015 The Funtasty. All rights reserved.
//

import JASON
import AERecord
import CoreData

protocol DecodableOnContext: Decodable, Identifiable { }

// Decodable protocol

protocol Decodable {
    typealias DecodedType = Self
    static func decode(object: DecodedType, json: JSON, context: NSManagedObjectContext)
}

extension CollectionType where Generator.Element: Identifiable, Generator.Element: Decodable, Generator.Element == Generator.Element.DecodedType {
    
    static func decode(j: JSON, context: NSManagedObjectContext) -> [Generator.Element] {
        let array: [Generator.Element?] = j.arrayValue.map { jsonObject in
            let json = JSON(jsonObject)
            
            if let primaryKeyValue = Generator.Element.primaryKeyValueFromJSON(json), let obj = Generator.Element.firstOrCreateWithPrimaryKeyValue(Generator.Element.primaryKey, value: primaryKeyValue, context: context) as? Generator.Element.DecodedType {
            
                Generator.Element.decode(obj, json: json, context: context)
                return obj
            } else {
                return nil
            }
        }
        
        return array.flatMap { $0 }
    }
}


// Identifiable protocol

protocol Identifiable {
    typealias ClassType = Self
    static var primaryKey: String { get }
    static func primaryKeyValueFromJSON(json: JSON) -> AnyObject?
    static func firstOrCreateWithPrimaryKeyValue(primaryKey: String, value: AnyObject, context: NSManagedObjectContext) -> ClassType
}

extension Identifiable {
    static func firstOrCreateWithPrimaryKeyValueIfValue(key: String, value: AnyObject?, context: NSManagedObjectContext) -> ClassType? {
        
        if let value = value {
            return self.firstOrCreateWithPrimaryKeyValue(key, value: value, context: context)
        } else {
            return nil
        }
    }
}

// Decoder

class Decoder {
    class func decode<T where T: Decodable, T: Identifiable, T == T.DecodedType, T == T.ClassType>(object: AnyObject?, context: NSManagedObjectContext) -> T? {
        
        guard let object = object else {
            return nil
        }

        let json = JSON(object)
        
        if let obj = T.firstOrCreateWithPrimaryKeyValueIfValue(T.primaryKey, value: T.primaryKeyValueFromJSON(json), context: context) {
            T.decode(obj, json: json, context: context)
            return obj
        } else {
            return nil
        }
    }
    
    class func decode<T where T: Decodable, T: Identifiable, T == T.DecodedType>(object: AnyObject?, context: NSManagedObjectContext) -> [T]? {
        guard let object = object else {
            return nil
        }
        return Array<T>.decode(JSON(object), context: context)
    }
    
}


