//
//  UserManager.swift
//  todo-list
//
//  Created by Aleš Kocur on 27/11/15.
//  Copyright © 2015 Aleš Kocur. All rights reserved.
//

import Locksmith

class Account {
    
    static let uniqueAccount = Account(service: "ToDoListAccount")
    
    var token: UserToken?
    var username: String?
    var account: String = "UserAccount"
    let service: String
    
    private let tokenKey = "token"
    private let usernameKey = "username"
    
    init(service: String) {
        self.service = service
        self.loadAccount()
    }
    
    func saveAccount(token: UserToken, username: String) {
        self.token = token
        self.username = username
        do {
            try Locksmith.saveData([tokenKey: token, usernameKey: username], forUserAccount: account, inService: service)
        } catch {
            print("** Error: Unable to save token to keychain!")
        }
    }
    
    func loadAccount() {
        if let acc = Locksmith.loadDataForUserAccount(account, inService: service) {
            self.token = acc[tokenKey] as? String
            self.username = acc[usernameKey] as? String
        }
    }
    
    func deleteAccount() {
        do {
            try Locksmith.deleteDataForUserAccount(account, inService: service)
        } catch {
            print("Deleting account failed")
        }
    }
}


class UserManager {

    class var accessToken: String? {
        return Account.uniqueAccount.token
    }
    
    class var isUserLogged: Bool {
        return UserManager.accessToken != nil
    }
    
    class var loggedUser: User? {
        
        if let username = Account.uniqueAccount.username {
            let user = User.firstOrCreateWithAttribute("username", value: username) as! User
            return user
        } else {
            return nil
        }
    }
    
    
    class func login(username: String, password: String, completion: (ResponseData<User>) -> ()) {
        
        UserServices.login(username: username, password: password) { (responseData) -> () in
            
            switch responseData {
            case .Data(let userBox):
                Account.uniqueAccount.saveAccount(userBox.token, username: username)
                
                completion(ResponseData.Data(userBox.user))
                
            case .Error(let error):
                print("Login error: \(error)")
                completion(ResponseData.Error(error))
            }
            
        }
        
    }
    
}
